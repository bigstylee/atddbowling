<?php 

namespace Bowling;

Class Game {

    private $rounds;

    private $result;

    function __construct($rounds)
    {
        $this->rounds = $rounds;
    }

    public function addThrows($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10)
    {
        $result = 0;
        foreach (func_get_args() as $score) {
            $result += $score[0] + $score[1];
        }

        $this->result = $result;
    }

    public function getResult()
    {
        return $this->result;
    }
}