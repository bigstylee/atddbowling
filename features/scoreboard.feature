Feature:
ScoreBoard showing the results of our bowling game



Scenario Outline: Calculate Game Score for Bowling Game
  Given I play a 10 rounds Bowling game
  When I bowl a game <game>
  Then the game result will be <result>
  Examples:
  | game                         | result |
  | 11 11 11 11 11 11 11 11 11 11| 20     |
  | 00 00 00 00 00 00 00 00 00 00| 0      |


Scenario Outline: Calculate a strike
  Given I get a strike on my first go
  When I bowl a game <game>
  Then the game result will be <result>
  Examples:
  | game                         | result |
  | X0 00 00 00 00 00 00 00 00 00| 20     |